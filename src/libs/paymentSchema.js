const Joi = require("@hapi/joi");

const paymentOptions = {
  CreditCard: "CreditCard",
  DebitCard: "DebitCard",
  Cash: "Cash",
  Transfer: "Transfer",
};

const paymentSchema = Joi.object({
  name: Joi.string().trim().min(3).max(50).required(),
  email: Joi.string().email().required(),
  career: Joi.string().allow("").optional(),
  dateOfBirth: Joi.date().allow("").optional(),
  phoneNumber: Joi.number().required(),
  country: Joi.string().trim().required(),
  city: Joi.string().trim().required(),
  paymentOption: Joi.string()
    .valid(...Object.values(paymentOptions))
    .required(),
  paymentInstallments: Joi.number().valid(1, 3, 6).required(),
});

module.exports = paymentSchema;
