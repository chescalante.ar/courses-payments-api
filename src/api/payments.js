const express = require("express");
const db = require("monk")(process.env.MONGO_DB);
const schema = require("../libs/paymentSchema");

const router = express.Router();

router.get("/", async (request, response) => {
  const payments = await db.get("payments");

  const result = await payments.find({});

  db.close();

  response.json(result);
});

router.post("/add-preference", async (request, response, next) => {
  try {
    const paymentPreference = await schema.validateAsync(request.body);

    const payments = await db.get("payments");

    const paymentPreferenceResult = await payments.insert(paymentPreference);

    db.close();

    response.json(paymentPreferenceResult);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
