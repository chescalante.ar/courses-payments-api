const express = require("express");

const payments = require("./payments");

const router = express.Router();

router.get("/", (req, res) => {
  res.json({
    message: "API - 👋🌎🌍🌏",
  });
});

router.use("/payments", payments);

module.exports = router;
