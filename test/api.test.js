process.env.NODE_ENV = "test";

const request = require("supertest");
const assert = require("assert");

const app = require("../src/app");

describe("GET /api/v1", () => {
  it("responds with a json message", (done) => {
    request(app)
      .get("/api/v1")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(
        200,
        {
          message: "API - 👋🌎🌍🌏",
        },
        done
      );
  });
});

describe("GET /api/v1/payments", () => {
  it("get all courses payments preferences", (done) => {
    request(app)
      .get("/api/v1/payments")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});

describe("POST /api/v1/payments/add-preference", () => {
  it("add a payment preference", (done) => {
    request(app)
      .post("/api/v1/payments/add-preference")
      .send({
        name: "Christian",
        email: "chescalante.ar@gmail.com",
        career: "Software Developer",
        dateOfBirth: new Date(1986, 10, 11),
        phoneNumber: 1165611110,
        country: "Argentina",
        city: "CABA",
        paymentOption: "Transfer",
        paymentInstallments: 1,
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
  });
});
