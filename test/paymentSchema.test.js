const { assert } = require("@hapi/joi");
const paymentSchema = require("../src/libs/paymentSchema");

describe("Payment Schema", () => {
  it("valid payment option (full)", () => {
    const coursePayment = {
      name: "Christian",
      email: "chescalante.ar@gmail.com",
      career: "Software Developer",
      dateOfBirth: new Date(1986, 10, 11),
      phoneNumber: 1165611110,
      country: "Argentina",
      city: "CABA",
      paymentOption: "Transfer",
      paymentInstallments: 1,
    };

    assert(coursePayment, paymentSchema, { abortEarly: false });
  });

  it("valid payment option (only required props)", () => {
    const coursePayment = {
      name: "Christian",
      email: "chescalante.ar@gmail.com",
      phoneNumber: 1165611110,
      country: "Argentina",
      city: "CABA",
      paymentOption: "Transfer",
      paymentInstallments: 1,
    };

    assert(coursePayment, paymentSchema, { abortEarly: false });
  });
});
